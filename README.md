![i3-pomodoro logo](assets/openlogos/logo+type.svg)
# i3-pomodoro

This is a small timer, which I first wrote to have a [pomodoro workflow](https://en.wikipedia.org/wiki/Pomodoro_Technique). There is not that much to say about it, but since I re-use images I didn't draw, I'd like to promote the artist.

In the `assets` folder, subfolder `openlogos`, you can find a file named `logo.svg`, drawn by *Aras Atasaygin* as part of his [OpenLogos project](https://github.com/arasatasaygin/openlogos). If you want to reuse the logo, please inform Aras before, at least to inform him that you like his work :-).

Prior to Aras logo, this project used the image *tomato.png*, available [here](https://openclipart.org/detail/215656/pixel-tomato), which is released under CC-0.

# Install & use

You need [rust](https://www.rust-lang.org/) to build this program. Once downloaded (either with `git` or direct download), run the following commands:
```
$ cd /path/to/folder/
$ cargo build
$ ./target/debug/i3-pomodoro [OPTIONS]
```

/!\ It is important to run the program from the base folder for the moment.

To know which options are available and how they work, use the `--help` option:

```
$ ./target/debug/i3-pomodoro --help
Pomodoro 0.1-α
Martin V. <martin@vassor.org>
A simple pomodoro timer

USAGE:
    i3-pomodoro [OPTIONS] --file <FILE> --task <TASK>...

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -f, --file <FILE>       Read task from provided todo.txt file
    -t, --task <TASK>...    Set the task message
    -T, --time <TIME>       Set the timer
```

One of `--task` or `--file` should be provided. If both are provided, the `--task` take precedence. Notice that you can provide multiple tasks with the same command line.

The file you can provide is a [todo.txt](https://github.com/todotxt/todo.txt-cli/wiki). This timer search for special tags `etc:duration` to estimate the time of completion of the task. The duration should have a nice format (i.e. if you provided a string with no colons nor blanks, it should work).

With `--time`, you can set the default timer you want. This timer is always used except when an `etc:duration` is provided. By default, the time interval is 15 minutes.

## Examples

The following example creates a notification after 10 minutes (i.e. 600 seconds) with the label "My task":

```
$ ./target/debug/i3-pomodoro --time=600 --task="My task"
```


The following example reads your `~/.todo/todo.txt` file and create an alert for each task, with default time interval of 15 minutes. However, the first task contains a tag `etc:10m`, hence its notification will be triggered after 10 minutes only.

```
$ cat ~/.todo/todo.txt
(A) Important task etc:10m
(B) Less important task
(C) Will you ever do this one ?
$ ./target/debug/i3-pomodoro --file ~/.todo/todo.txt
```

# Licence

Except for the *logo.svg* (and *logo+type.svg*) and the former logo *tomato.png* mentionned above, all the content of this repository is available under the [WTFPL](https://en.wikipedia.org/wiki/WTFPL).
