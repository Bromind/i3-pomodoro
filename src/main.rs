extern crate notify_rust;
extern crate clap;
extern crate todo_txt;
extern crate humantime;
use notify_rust::Notification;
use std::{thread, time};
use clap::{Arg, App};
use std::io::BufReader;
use std::fs::File;
use std::error::Error;
use std::io::BufRead;
use std::str::FromStr;
use todo_txt::Task;
use humantime::parse_duration;

fn notify(msg: String) {
    let mut noti = Notification::new()
        .appname("Pomodoro")
        .summary("Time is over")
        .body(msg.as_str())
        .timeout(-1)
        .clone();

    match std::env::current_exe() {
        Ok(mut p) => {
            p.pop(); // Remove executable name
            let path = &(p.to_str().unwrap().to_owned() + "/../../assets/openlogos/logo.svg");
            noti.icon(path)
        },
        Err(_) => &mut noti,
    };

    match noti
        .finalize()
        .show() {
        Ok(_) => (),
        Err(_) => println!("D-bus error"),
    }
}

fn wait_and_alert(msg: String, time: u64) {
    let duration = time::Duration::new(time, 0);
    thread::sleep(duration);

    notify(msg);
}

fn tasks_from_cli(tasks: clap::Values, timer: u64) {
    for a in tasks {
        wait_and_alert(a.to_string(), timer)
    }
}

fn tasks_from_file(filename: String, timer: u64) {
    let file = match File::open(filename) {
        Ok(f) => f,
        Err(e) => panic!("{}", e.description()),
    };
    let file_buf = BufReader::new(file);
    for line in file_buf.lines() {
        let line_str = &line
            .expect("Can not read file.");
        let task = Task::from_str(line_str)
            .expect(&format!("Can not construct a tast from the line:\n\t{}", line_str));
        let etc = task.tags
            .get("etc")
            .map_or(timer, |s| parse_duration(s)
                    .map(|d| d.as_secs())
                    .unwrap_or(timer)
                    );
        let subject = task.subject;
        wait_and_alert(subject, etc)
    }
}

fn main() {
    let matches = App::new("Pomodoro")
        .version("0.1-α")
        .author("Martin V. <martin@vassor.org>")
        .about("A simple pomodoro timer")
        .arg(Arg::with_name("task")
             .short("t")
             .long("task")
             .value_name("TASK")
             .required_unless("file")
             .multiple(true)
             .help("Set the task message")
            )
        .arg(Arg::with_name("time")
             .short("T")
             .long("time")
             .value_name("TIME")
             .help("Set the timer")
            )
        .arg(Arg::with_name("file")
             .short("f")
             .long("file")
             .value_name("FILE")
             .required_unless("task")
             .help("Read task from provided todo.txt file")
            )
        .get_matches();

    let timer = 
        match matches.value_of("time")
        {
            Some(s) => s.parse::<u64>()
                .expect("Given time could not be parsed"),
            None => 15 * 60
        };

    match matches.values_of("task") {
        Some(tasks) => tasks_from_cli(tasks, timer),
        None => tasks_from_file(matches
                            .value_of("file")
                            .unwrap_or_else(|| panic!("No task or file found"))
                            .to_string(),
                            timer),
    }
}
